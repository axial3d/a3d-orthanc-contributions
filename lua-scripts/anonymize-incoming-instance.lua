-- Anonymize all incoming DICOM files

function OnStoredInstance(instanceId, tags, metadata, origin)
        if origin['RequestOrigin'] ~= 'Lua' then
                local replace = {}
                replace['PatientName'] = origin['RemoteAet'] .. '-' .. string.sub(tags['PatientBirthDate'],1,4) .. '-' .. tags['PatientSex']
                replace['PatientID'] = replace['PatientName']
                replace['StudyInstanceUID'] = tags['StudyInstanceUID'] .. '-anonymized'
                replace['SeriesInstanceUID'] = tags['SeriesInstanceUID'] .. '-anonymized'
                replace['SOPInstanceUID'] = tags['SOPInstanceUID'] .. '-anonymized'
                local keep = {
                        "SeriesDescription",
                        "StudyDescription",
                        "PatientSex"
                }
                local command = {}
                command['Replace'] = replace
                command['Keep'] = keep
                command['Force'] = true
                local anonymized = RestApiPost('/instances/' .. instanceId .. '/anonymize', DumpJson(command, true))
                RestApiDelete('/instances/' .. instanceId)
                RestApiPost('/instances/', anonymized)
        end
end

